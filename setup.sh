#!/usr/bin/env bash

if [ -n "$(command -v yum)" ]; then
    sudo yum install -y postfix
    sudo yum install python python-dev python-pip
    sudo cat > /etc/yum.repos.d/mongodb-org.repo <<EOF
[mongodb-org-3.4]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.4/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-3.4.asc

EOF
    sudo yum install -y mongodb-org
    sudo systemctl start mongod
else
    sudo apt-get install -y postfix
    sudo apt-get install python python-dev python-pip
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
    echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
    sudo apt-get update
    sudo apt-get install -y mongodb-org
    sudo systemctl start mongod
fi

sudo service postfix start

sudo sed -i -- 's/virtual_alias_maps = /#virtual_alias_maps = /g' /etc/postfix/main.cf
sudo sed -i -- 's/alias_maps = /#alias_maps = /g' /etc/postfix/main.cf
sudo sed -i -- 's/alias_database = /#alias_database = /g' /etc/postfix/main.cf

sudo echo "virtual_alias_maps = hash:/etc/postfix/virtual" | sudo tee -a /etc/postfix/main.cf
sudo echo "alias_maps = hash:/etc/aliases" | sudo tee -a /etc/postfix/main.cf
sudo echo "alias_database = hash:/etc/aliases.db" | sudo tee -a /etc/postfix/main.cf


cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"
sudo echo "dac_receiver: \"|${cur_dir}/receiver.py\"" | sudo tee -a /etc/aliases

sudo touch /etc/postfix/virtual
sudo echo "youremail@example.org dac_receiver" | sudo tee -a /etc/postfix/virtual

sudo postmap /etc/postfix/virtual
sudo newaliases
sudo postalias /etc/aliases
sudo postfix reload

sudo pip install pymongo