# Document Access Control system

## Install **postfix** and configure.

    bash setup.sh

## Send email with attachment on command line:
        
        echo "This is the body of the email" | mail -s "This is the subject" youremail@example.org -A /home/shane/send.txt
        
        echo "This is the body of the email" | mail -s "This is the subject" youremail@example.org --content-type "image" -A /home/shane/Pictures/1.jpg