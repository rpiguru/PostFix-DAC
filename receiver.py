#!/usr/bin/env python
import json
import sys
import email
import os
import subprocess
import time
from pymongo import MongoClient


if len(sys.argv) > 1:
    in_str = open('result_img.txt').read()
else:
    in_str = sys.stdin.read()

msg = email.message_from_string(in_str)

mongo_client = MongoClient('localhost', 27017)
db = mongo_client['DAC']
col_dac = db['dac']


metadata = {k: v for k, v in msg.items()}
"""
Example metadata:
    {
        'Content-Type': 'multipart/mixed; boundary="877240018-1514448206=:5764"',
        'Date': 'Thu, 28 Dec 2017 18:03:26 +1000 (AEST)',
        'Delivered-To': 'script@example.org',
        'From': 'shane@shane-desktop (Shane Carlyon)',
        'MIME-Version': '1.0',
        'Message-Id': '<20171228080326.8DF0610459A@example.org>',
        'Received': 'by example.org (Postfix, from userid 1000)\n\tid 8DF0610459A; Thu, 28 Dec 2017 18:03:26 +1000 (AEST)',
        'Return-Path': '<shane@shane-desktop>',
        'Subject': 'This is the subject',
        'To': '<youremail@example.org>',
        'X-Mailer': 'mail (GNU Mailutils 2.99.99)',
        'X-Original-To': 'youremail@example.org'
    }
"""

content = msg.get_payload()[0]
mail_body = content.get_payload()

# TODO: Check mail body if it is from the manager.

dest_dir = os.path.join(os.path.dirname(__file__), 'from_{}_{}'.format(metadata['From'], time.time()))
if not os.path.exists(dest_dir):
    os.makedirs(dest_dir)

metadata['files'] = []

for i, attachment in enumerate(msg.get_payload()[1:]):
    content_type = attachment.get_content_type()
    file_name = attachment.get_filename()
    metadata['files'].append(file_name)
    open(os.path.join(dest_dir, file_name), 'wb').write(attachment.get_payload(decode=True))

with open(os.path.join(dest_dir, 'metadata.json'), 'w') as meta_file:
    json.dump(metadata, meta_file, indent=2)

subprocess.call(['chmod', '-R', '666', dest_dir + '/'])

metadata['state'] = 'pending'
metadata['body'] = mail_body
col_dac.insert_one(metadata)
