import threading
import time
from pymongo import MongoClient

mongo_client = MongoClient('localhost', 27017)
db = mongo_client['DAC']
col_dac = db['dac']


class PostfixManager(threading.Thread):

    b_stop = threading.Event()

    def __init__(self):
        super(PostfixManager, self).__init__()
        self.b_stop.clear()

    def run(self):
        while not self.b_stop.isSet():
            mail = col_dac.find({'state': 'pending'})
            if mail:
                print('Sending an email to the manager')
                col_dac.update_one({"_id": mail['_id']}, {"$set": {"state": 'wait_confirm'}})
            mail = col_dac.find({'state': 'approved'})
            if mail:
                print('Sending an email to the customer.')
                col_dac.update_one({"_id": mail['_id']}, {"$set": {"state": 'done'}})

            time.sleep(.5)

    def stop(self):
        self.b_stop.set()


if __name__ == '__main__':

    manager = PostfixManager()
    manager.start()
